#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

ComputeTemperature::ComputeTemperature(Real timestep,Real ro,Real c,Real k) : dt(timestep),ro(ro),c(c),k(k) {}

void ComputeTemperature::compute(System& system) 
{
    Real length = 2.0;
    // get number of point
    UInt noParticle = system.getNbParticles();
    UInt sizeMatrix = sqrt(noParticle);


    // create matrix use for solving
    Matrix<complex> theta(sizeMatrix);
    Matrix<complex> heatRateHat(sizeMatrix);
    Matrix<complex> heatSource(sizeMatrix);

    // run over all the particle and fill the matrix
    for(UInt i = 0;i<sizeMatrix;i++)
    {
        for(UInt j = 0;j<sizeMatrix;j++)
        {
            MaterialPoint& pt = static_cast<MaterialPoint&>(system.getParticle(i*sizeMatrix+j));
            theta(i,j) = pt.getTemperature();
            heatSource(i,j) = pt.getHeatSource();
        }
    }

    // Coordinate in fourier space
    Matrix<std::complex<int>> waveNumber = FFT::computeFrequencies(sizeMatrix);

    // fourier transform
    Matrix<complex> thetaHat = FFT::transform(theta);
    Matrix<complex> heatSourceHat = FFT::transform(heatSource);

    // heat equation solve  
    for (UInt i = 0; i < sizeMatrix; i++) 
    {
        for (UInt j = 0; j < sizeMatrix; j++) 
        {
            // Real q_x = std::real(waveNumber(i,j)) * 2.0 * M_PI/length;
            // Real q_y = std::imag(waveNumber(i,j)) * 2.0 * M_PI/length;
            Real coo = std::abs(waveNumber(i,j)) * 2.0 * M_PI/length;
            coo *= coo;

            heatRateHat(i,j) = 1./(this->ro*this->c) * (heatSourceHat(i,j) - this->k * thetaHat(i,j) * coo);
            //std::cout << i << " " << j << " 1 "<< heatSourceHat(i,j) << " 2 " << thetaHat(i,j) << std::endl;
            if (i==0)
                //std::cout << i << " " << j << " 1 "<< heatSourceHat(i,j) << " 2 " << this->k * thetaHat(i,j) * coo << "3" << heatRateHat(i,j) << std::endl;
                std::cout << i << " " << j << " ::: " << heatRateHat(i,j) << std::endl;
            //std::cout << i << " " << j << " qx "<< q_x <<" qy "<< q_y << std::endl;
        }
    }

    //fourier inverse
     Matrix<complex> heatRate = FFT::itransform(heatRateHat);

    // stor back temperature in material point
    for (UInt i = 0; i < sizeMatrix; i++) 
    {
        for (UInt j = 0; j < sizeMatrix; j++) 
        {
            MaterialPoint& pt = static_cast<MaterialPoint&>(system.getParticle(i*sizeMatrix+j));
            //if (i == 0)
                //std::cout << i << " " << j << " T "<< pt.getTemperature() << " HS " << pt.getHeatSource()<< " HR " << heatRate(i,j).real() << std::endl;
            pt.getTemperature() += this->dt * heatRate(i,j).real();

        }
    }

}

/* -------------------------------------------------------------------------- */
