

#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "material_point.hh"
#include "system.hh"
#include <gtest/gtest.h>



// Fixture class
class ReadTest : public ::testing::Test {
protected:
  void SetUp() override 
  {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> points;
    n_points = 32*32;

    for (UInt i = 0; i < n_points; ++i) 
    {
      MaterialPoint p;
      p.getTemperature() = 20;
      p.getHeatSource() = 0;
      points.push_back(p);
    }

    for (auto& p : points) {
      // std::cout << p << std::endl;
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }
  }

  System system;
  UInt n_points;
};

TEST_F(ReadTest, csv) {
  CsvWriter writer("tmp_file");
  writer.compute(system);

  CsvReader reader("tmp_file");
  System s_fromfile;
  reader.compute(s_fromfile);

  UInt read_n_points = s_fromfile.getNbParticles();
  EXPECT_EQ(n_points, s_fromfile.getNbParticles());
  

  for (UInt i = 0; i < n_points; ++i) 
  {
    MaterialPoint* part1 = dynamic_cast<MaterialPoint*>(&system.getParticle(i));
    auto& hs1 = part1->getHeatSource();
    auto& t1 = part1->getTemperature();
    MaterialPoint* part2 = dynamic_cast<MaterialPoint*>(&s_fromfile.getParticle(i));
    auto& hs2 = part2->getHeatSource();
    auto& t2 = part1->getTemperature();
    
    ASSERT_NEAR(hs1, hs2, std::abs(hs1) * 1e-12);
    ASSERT_NEAR(t1, t2, std::abs(t1) * 1e-12);
  }  
}

// Fixture class
class RestSteadyState : public ::testing::Test {

protected:
  void SetUp() override 
  {
    n_points = 512;

    for (UInt i = 0; i < n_points*n_points; ++i) 
    {
      MaterialPoint p;
      p.getTemperature() = 20;
      p.getHeatSource() = 0;
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }

    tempFFT = std::make_shared<ComputeTemperature>(1.);

  }

  System system;
  UInt n_points;

  std::shared_ptr<ComputeTemperature> tempFFT;
};
TEST_F(RestSteadyState, test_rest_steady_state) 
{

  tempFFT->compute(system);

  for (UInt i = 0; i < n_points*n_points; ++i) 
  {
    MaterialPoint* matPoint = dynamic_cast<MaterialPoint*>(&system.getParticle(i));
    // std::cout << i << " " << matPoint->getTemperature() << std::endl;

    ASSERT_NEAR(matPoint->getTemperature(), 20 , 20 * 1e-12);
  }
}



// Fixture class
class ThermalEvolution : public ::testing::Test {
protected:
  void SetUp() override 
  {
    n_points = 7;
    gideLength = 2.;


    for (UInt i = 0; i < n_points*n_points; ++i) 
    {
      MaterialPoint p;

      p.getTemperature() = sin((2.0*M_PI)*((Real(i%n_points))/Real(n_points-1)));
      p.getHeatSource() = pow(2.0*M_PI/gideLength,2.)*sin((2.0*M_PI)*((Real(i%n_points))/Real(n_points-1)));
      system.addParticle(std::make_shared<MaterialPoint>(p));

      //std::cout << "i:"<< i << " T:" << p.getTemperature() << " H:"<< p.getHeatSource() << std::endl;
    }

    tempFFT = std::make_shared<ComputeTemperature>(1.,1,1,1);

  }

  System system;
  UInt n_points;
  Real gideLength;

  std::shared_ptr<ComputeTemperature> tempFFT;
};

TEST_F(ThermalEvolution, test_sin_state) 
{
  UInt nStep = 1;

  for(UInt i = 0;i<nStep;i++)
    tempFFT->compute(system);

  for (UInt i = 0; i < n_points; ++i)
  {
      MaterialPoint* matPoint = dynamic_cast<MaterialPoint*>(&system.getParticle(i));
  }

  for (UInt i = 0; i < n_points*n_points; ++i) 
  {
    MaterialPoint* matPoint = dynamic_cast<MaterialPoint*>(&system.getParticle(i));

    Real tmp = 20.+sin((2.0*M_PI)*((Real(i%n_points))/Real(n_points-1)));
    // std::cout << i << " " << matPoint->getTemperature() << std::endl;
    // std::cout << i << " " << matPoint->getTemperature() << "##->" << tmp << std::endl;
    ASSERT_NEAR(matPoint->getTemperature(), tmp , tmp* 1e-12);
  }
}


/*
// Fixture class
class ThermalEvolution2 : public ::testing::Test {
protected:
  void SetUp() override 
  {
    n_points = 7;
    gideLength = 2.;


    for (UInt i = 0; i < n_points*n_points; ++i) 
    {
      MaterialPoint p;

      p.getTemperature() = sin((2.0*M_PI)*((Real(i%n_points))/Real(n_points-1)));
      p.getHeatSource() = pow(2.0*M_PI/gideLength,2.)*sin((2.0*M_PI)*((Real(i%n_points))/Real(n_points-1)));
      system.addParticle(std::make_shared<MaterialPoint>(p));

      //std::cout << "i:"<< i << " T:" << p.getTemperature() << " H:"<< p.getHeatSource() << std::endl;
    }

    tempFFT = std::make_shared<ComputeTemperature>(1.);

  }

  System system;
  UInt n_points;
  Real gideLength;

  std::shared_ptr<ComputeTemperature> tempFFT;
};

TEST_F(ThermalEvolution2, test_sin_state) 
{
  UInt nStep = 1;

  for(UInt i = 0;i<nStep;i++)
    tempFFT->compute(system);

  for (UInt i = 0; i < n_points; ++i)
  {
      MaterialPoint* matPoint = dynamic_cast<MaterialPoint*>(&system.getParticle(i));
  }

  for (UInt i = 0; i < n_points*n_points; ++i) 
  {
    MaterialPoint* matPoint = dynamic_cast<MaterialPoint*>(&system.getParticle(i));

    Real tmp = 20.+sin((2.0*M_PI)*((Real(i%n_points))/Real(n_points-1)));
    // std::cout << i << " " << matPoint->getTemperature() << std::endl;
    // std::cout << i << " " << matPoint->getTemperature() << "##->" << tmp << std::endl;
    ASSERT_NEAR(matPoint->getTemperature(), tmp , tmp* 1e-12);
  }
}
*/