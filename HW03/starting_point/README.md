# Sp4e Homework 3

## Description
The primary objective is to adapt an external scientific computing library (FFTW) for specific project needs through a wrapping interface. The project includes a heat equation solver using FFTW and a system for simulating various particle interactions and dynamics.

## Requirements
Minimum requirements and compile options are listed below:
For C++:
- cmake_minimum_required (VERSION 2.8.11)
- add_compile_options(-std=c++14)
- FFTW version 3.3.10
- GoogleTest v1.12.1

For Python:
- numpy v.1.23.4
- matplotlib v.3.6.1
- csv v.1.0

## Usage
### Structure:

Use the included Doxyfile with doxywizard to generate documentation. The generated files will be placed in the /html directory.

### Particle Organization:

* MaterialPointsFactory and MaterialPoint: These are child classes of ParticlesFactoryInterface and Particle, respectively. They are used to create and manage material points in the simulation.


* Matrix and FFT: The Matrix struct is a template class for 2D matrix manipulation, and the FFT struct is used for operations involving the FFTW library, particularly in the context of Fourier transforms and heat equation solving.

### Running the program
Build the program as the following: 
```
cd starting_point
mkdir build
cd build 
cmake ..
make
```


### Interface Implementation
FFTW wrapping interface has been implemented and tests are prepared to verify the results.

**Executing test of FFTW implementation:**
```cpp
cd build
./test_fft
```

### Solver Implementation
A solver for a transient heat equation is implemented and tests are prepared to verify the results.

**Executing test of temperature computation implementation:**
```cpp
cd build
./test_temp
```
------


## Authors
Cyrille Masserey and Hung-Wei Li