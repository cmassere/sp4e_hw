#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) 
{
  auto rows = m_in.rows();
  auto cols = m_in.cols();
  
  auto size = m_in.size();
  Matrix<complex> m_out(size);

  auto in = (fftw_complex*) m_in.data();
  auto out = (fftw_complex*) m_out.data();

  // Create FFTW plan
  fftw_plan plan = fftw_plan_dft_2d(rows, cols, 
  in, 
  out, 
  FFTW_FORWARD, 
  FFTW_ESTIMATE);

  // Execute the plan
  fftw_execute(plan);

  // Clean up
  fftw_destroy_plan(plan);
  // Return the transformed matrix
  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) 
{

  auto rows = m_in.rows();
  auto cols = m_in.cols();
  
  auto size = m_in.size();
  Matrix<complex> m_out(size);

  auto in = (fftw_complex*) m_in.data();
  auto out = (fftw_complex*) m_out.data();

  // Create FFTW plan
  fftw_plan plan = fftw_plan_dft_2d(rows, cols, 
  in, 
  out, 
  FFTW_FORWARD, 
  FFTW_ESTIMATE);

  // Execute the plan
  fftw_execute(plan);

  // Clean up
  fftw_destroy_plan(plan);

  // Normalize and return the transformed matrix
  // output /= (rows * cols);
  return m_out /= (rows * cols);
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {

  Matrix<std::complex<int>> m_out(size);
  UInt limit;
  int real_num, imag_num;
  // First check if the window size is even or odd.
  if (size % 2 == 0) {
    limit = (size / 2) - 1;
  } else {
    limit = (size - 1) / 2;
  };

  for (auto&& entry : index(m_out)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (i <= limit) {
      real_num = i;
      if (j <= limit) {
        imag_num = j;
      } else {
        imag_num = j - size;
      }
    } else {
      real_num = i - size;
      if (j <= limit) {
        imag_num = j;
      } else {
        imag_num = j - size;
      }
    }

    val = std::complex<int>(real_num, imag_num);
  }

  return m_out;
}
#endif  // FFT_HH
