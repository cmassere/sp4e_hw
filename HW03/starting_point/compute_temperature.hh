#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

  // Virtual implementation
public:
  ComputeTemperature(Real timestep,Real ro=1,Real c=1,Real k=1);

  //! Penalty contact implementation
  void compute(System& system) override;

private:
  Real dt;
  Real ro = 1;
  Real c = 2;
  Real k = 1;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
