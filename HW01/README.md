# Quadratic Function Optimizer

- [Quadratic Function Optimizer](#quadratic-function-optimizer)
  - [Files Description](#files-description)
    - [`main.py`](#mainpy)
    - [`optimizer.py`](#optimizerpy)
    - [`visualize.py`](#visualizepy)
    - [`GMRES.py`](#gmrespy)
  - [Usage Guide](#usage-guide)
  - [Notes](#notes)
  - [Dependencies](#dependencies)


This project provides a tool to optimize a quadratic function using various optimization methods. The tool provides a command-line interface to specify the quadratic function and the optimization method. It also provides a visualization tool to view the function and the optimization path in 3D.

## Files Description

### `main.py`
The main script to run the optimization process. It:
- Parses command-line arguments.
- Runs the chosen optimization method.
- Optionally visualizes the result.

### `optimizer.py`
Contains the optimization routines:
- Definition of the quadratic function.
- Available optimization methods: 'BFGS', 'LGMRES'.

### `visualize.py`
Provides tools to:
- Visualize the quadratic function in 3D.
- Plot the optimization path.

### `GMRES.py`
This file contains the GMRES optimization method developed following the instructions of exercise 2.

## Usage Guide

How to Use:

1. **Optimizing a Quadratic Function** : 
   Run the `main.py` script with command-line arguments specifying the quadratic function and the optimization method.

```
python main.py --A 8 1 1 3 --b 2 4 --method LGMRES
```

Where:
- `--A`: Represents the flattened matrix A.
- `--b`: Is the Vector b.
- `--method`: Refers to the optimization method, with choices being ['GMRES', 'BFGS', 'LGMRES'].
- `--plot`: An optional flag that triggers visualization of the quadratic function and the optimization path in 3D.



2. **Visualization**: 
   To visualize the result, simply add the `--plot` flag:

```
python main.py --A 8 1 1 3 --b 2 4 --method LGMRES --plot
```

## Notes
- The `LGMRES` optimization method is sourced directly from the package `scipy.sparse.linalg.lgmres`.

## Dependencies
- `numpy`
- `scipy`
- `matplotlib`
