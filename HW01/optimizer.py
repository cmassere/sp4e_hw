# optimizer.py

import numpy as np
import GMRES
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres

def quadratic_function(x, A, b):
    return  0.5 * x.T @ A @ x - x.T @ b


# def gradient_quadratic(x, A, b):
#     return A @ x - b


def optimizer_functor(A, b, method):

    x0 = [-10, -10]
    path = [x0]  # to store optimization path

    def callback(xk):
        path.append(np.copy(xk))
    
    if method == 'BFGS':

        result = minimize(
        fun = quadratic_function, 
        x0 = x0,  # initial guess
        args = (A, b), 
        method = method, 
        callback = callback

        )

        return result.x, np.array(path)

    elif method == 'LGMRES':
        # x0 = np.zeros_like(b)

        result, _ = lgmres(A, b, x0 = x0, callback = callback )
        return result, np.array(path)
    
    
    elif method == 'GMRES':
        result = GMRES.my_gmres(A, b, x0 = x0, callback = callback )
        return result, np.array(path)

    else:
        raise ValueError("Invalid method provided.")
