import numpy as np

##################################  !!! TO READ !!!  ###################################
# Since neither of us is already familiar with this algorithm, and our last linear     #
# algebra course was six years ago, I've spent about 95% of my time (nearly 8 hours)   #
# trying to understand this method instead of coding. Even now, I'm not entirely sure  #
# if everything is working correctly because I don't fully grasp the algorithm, and    #
# some checks that should be present in such a complex algorithm have been removed to  #
# simplify the resolution.                                                             #
########################################################################################

def my_MatMult(A,B):
    return np.einsum("ik,kj->ij",A,B)
def my_Dot(A,x):
    return np.einsum("ik,k->i",A,x)
def my_VectMult(a,b):
    return np.einsum("k,k->",a,b)

def my_gmres(A,b,x0=None,callback=None,max_iter=None,tol=1e-13):
    n = A.shape[0]
    m = max_iter

    if m == None:
        m = n+1
    
    x0 = np.zeros(n)

    r = b-my_Dot(A,x0)
    norm_b = np.linalg.norm(b,2)
    norm_r = np.linalg.norm(r,2)

    Kn = np.reshape(b/norm_b,(b.shape[0],1))
    Hn = np.array([[]])
    Qn = np.reshape(b/norm_b,(b.shape[0],1))

    for k in range(1,m):
        v = my_Dot(A, Qn[:,k-1])
        h_tmp = np.zeros((k+1,1))
        for j in range(k):
            h_tmp[j,0] = my_VectMult(Qn[:,j],v)
            v = v - h_tmp[j,0]*Qn[:,j]

        h_tmp[k,0] = np.linalg.norm(v,2)
        Hn = np.concatenate([Hn,np.zeros((1,Hn.shape[1]))])
        Hn = np.concatenate([Hn, h_tmp], axis=1)
        Qn = np.concatenate([Qn,np.reshape(v/h_tmp[k,0],(Qn.shape[0],1))], axis=1)

        e1 = np.zeros(Hn.shape[0])
        e1[0] = norm_r

        y, _, _, _ = np.linalg.lstsq(Hn, e1, rcond=None)

        x = my_Dot(Qn[:,:k], y)

        if callback != None:
            callback(np.asarray(x))
        
        residual = np.linalg.norm(b-my_Dot(A,x))
        # Check for convergence
        if residual < tol:
            return x

    return x