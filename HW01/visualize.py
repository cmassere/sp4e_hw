import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import optimizer


def plot_quadratic_function (A, b):

    x1 = np.linspace(-10, 10, 100)
    x2 = np.linspace(-10, 10, 100)

    X1, X2 = np.meshgrid(x1, x2)
    Z = np.zeros(X1.shape)

    # Calculate Z values using the quadratic function
    for i in range(X1.shape[0]):
        for j in range(X1.shape[1]):
            x = np.array([X1[i, j], X2[i, j]])
            Z[i, j] = optimizer.quadratic_function(x, A, b)

    # Plotting
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X1, X2, Z, cmap='viridis', alpha=0.6)

    ax.set_xlabel('X1 axis')
    ax.set_ylabel('X2 axis')
    ax.set_zlabel('Z axis')

    plt.show()


def plot_function(A, b, minimizer, path):

    x = np.linspace(-10, 10, 400)
    y = np.linspace(-10, 10, 400)
    X, Y = np.meshgrid(x, y)
    Z = np.array([[optimizer.quadratic_function(np.array([xi, yi]), A, b) for xi in x] for yi in y])


    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, Z, cmap='viridis', alpha=0.6)

    # plot the optimization path
    ax.plot(path[:, 0], path[:, 1], [optimizer.quadratic_function(np.array([xk[0], xk[1]]), A, b) for xk in path], 'r-o', label="Optimization Path")
    ax.scatter(minimizer[0], minimizer[1], optimizer.quadratic_function(minimizer, A, b), color='b', s=100, label="Minimizer")

    ax.set_xlabel('X axis')
    ax.set_ylabel('Y axis')
    ax.set_zlabel('Z axis')
    
    ax.legend()
    plt.show()

