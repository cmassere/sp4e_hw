import numpy as np
import optimizer

import visualize
import argparse


def main ():

    parser = argparse.ArgumentParser(description="Optimize a quadratic function")
    parser.add_argument('--A', nargs='+', type=float, default=[8, 1, 1, 3], help='Flattened matrix A.')
    parser.add_argument('--b', nargs='+', type=float, default=[2, 4], help='Vector b.')
    parser.add_argument('--method', type=str, choices=['GMRES', 'BFGS', 'LGMRES'], default='LGMRES', help='Optimization method.')
    parser.add_argument('--plot', action='store_true', help='Flag to plot the function.')

    args = parser.parse_args()

    A = np.array(args.A).reshape(2, 2)
    b = np.array(args.b)


    minimizer, path = optimizer.optimizer_functor(A, b, method=args.method)  # This function needs to return both minimizer and path

    print("minimum point:\n",minimizer)
    print("path:\n",path)
    if args.plot:
        visualize.plot_function(A, b, minimizer, path)
    # minimizer, path = optimizer.optimizer_functor(A, b)
    # visualize.plot_function(A, b, minimizer, path)

    # visualize.plot_function(A, b, method='BFGS')


if __name__ == "__main__":
    main()