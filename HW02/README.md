# Series Computation

This project computes various mathematical series and can output the results to the screen or to a file. The project also includes a Python script for reading files.

## Structure

The project includes the following components:

- `ComputeArithmetic` for arithmetic series calculations.
- `ComputePi` for computing Pi to a given precision using a series.
- `DumperSeries` as an interface for dumping series output.
- `PrintSeries` and `WriteSeries` for outputting series data to the console or to a file, respectively.
- `RiemannIntegral` for computing the Riemann integral of a function over an interval.
- `Series` as a base class for series computations.
- `ReadFiles.py` as a utility script for reading output files.

## Building the Project

To compile the project, run the following commands from the root of the repository:

```bash
mkdir build
cd build
cmake ..
make
```

## Wrok asignment
We have divided the work based on the weekly schedule and the experience of the people involved in the group.

Since Cyrille has more experience, he can start working immediately on the homework. As the series class and its daughter classes are crucial to this project and need to be completed as soon as possible, Cyrille will create these classes.

Hung-Wei, who also works with classes, will focus on the dummy series class and its daughter classes. If Cyrille isn't progressing quickly enough, Hung-Wei can begin working on the requested Python scripts.

The tasks related to stream manipulation and the argument parser were initially set aside and distributed later. Ultimately, Hung-Wei will handle stream manipulation, and Cyrille will take care of the argument parser.

## Code Complexity
The overall code complexity depends on the version and the specific task you want to perform. In its initial version (before exercise 5):
- The computation complexity for the series was O(n).
- The complexity for dumping the series was O(n!).

For the latest version, it still depends on the task at hand, but it also relies on the interpretation of the subject. Here's what we know for certain:
- For all series, the computation complexity remains O(n).
- Dumping 'ComputePi' and 'ComputeArithmetic' has a complexity of O(n). 

For Riemann, it depends on the interpretation:
- If we dump for multiple n values, the complexity stays at O(n!).
- If we first select the number of steps and then dump after a certain step, the complexity becomes O(n).

It was impossible to determine your precise requirements for this part, so we have chosen the second option.

## Argument Parsing

Once again, we are uncertain about the desired complexity of you ask for. Here what we have done:

- `'--step'` or `'-n'` to specify the number of steps you want to perform. The default value is 100.
- `'--series'` or `'-s'` to select the series you want from the options: `'arithmetic'`, `'pi'`, and `'riemann.'` The default value is `'arithmetic.'`
- `'--function'` or `'-f'` to choose the function you want to compute in the case of a Riemann integral. Select from `'3dg'`, `'sin'`, or `'cos'`. Default: `'3dg'`.
- `'--write'` or `'-w'` to write the result in a file.
- `'--print'` or `'-p'` to print the result in a file.
- `'--separator'` or `'-sep'` to choose the separator for the writing file. Options include `','`, `' '`, `'|'`, and `'\t'`. The default is `' '`.
- `'--value_a'` or `'-va'` to specify the first value of the integral. The default depends on the selected function (as requested in the subject).
- `'--value_b'` or `'-vb'` to specify the last value of the integral. The default also depends on the selected function (as requested in the subject).
- `'--frequency'` or `'-freq'` to specify the frequency of the PrintSeries.

