#include "Series.h"

#ifndef RIEMANNINTEGRAL_H
#define RIEMANNINTEGRAL_H

class RiemannIntegral:public Series
{
    private:
        SeriesInt a;
        SeriesReal b;
        SeriesReal(*func)(SeriesReal);
        SeriesInt n_iter;
        SeriesReal deltaX;

    public:
        RiemannIntegral(SeriesReal(*func)(SeriesReal),SeriesReal a = 0,SeriesReal b=1,SeriesInt n_iter=100);
        void setFunction(SeriesReal(*func)(SeriesReal));
        void setLimit(SeriesReal a,SeriesReal b);
        void setIter(SeriesInt n_iter);
        SeriesReal computeNextStep(void) override;
        SeriesReal getActualValue(void) override;
        SeriesReal compute(SeriesInt n);
};

#endif