#include <cstdlib>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include "Series.h"

#ifndef ARGPARS_H
#define ARGPARS_H

/*************************************************************************************
*                                                                                    *
*                                  Argument Parser                                   *
*                                                                                    *
**************************************************************************************
* We acknowledge that creating an independent class for the argument parser could    *
* result in a cleaner and more indicative representation of a profound understanding *
* of object-oriented and C++ programming. However, considering the overall           *
* complexity of this homework, the development time needed to implement such a       *
* class, and its complexity, we consider it outside the scope of this assignment.    *
* Consequently, we are presenting this class as a superficial one created for        *
* readability reasons.                                                               *
*                                                                                    *
* We are aware that some C++ users might provide a class for this purpose, but we do *
* not believe that it aligns with the requirements of this homework.                 *
*************************************************************************************/

enum SeriesType{
    _se_arithmetic,
    _se_pi,
    _se_riemann
};

enum FunctionType{
    _fct_3dg,
    _fct_cos,
    _fct_sin
};

enum ArgParsState{
    _APSt_none,
    _APSt_get_n,
    _APSt_series,
    _APSt_sep,
    _APSt_val_a,
    _APSt_val_b,
    _APSt_freq,
    _APSt_func
};

class ArgPars
{
    private:
        bool value_a_set = false;
        bool value_b_set = false;
    public:
        SeriesType seSel = _se_arithmetic;
        FunctionType fctSel = _fct_3dg;
        SeriesInt n = 100;
        SeriesReal a = 0;
        SeriesReal b = 1;
        SeriesReal freq = 10;
        std::string dumpSeparator = " ";
        bool writeSeries = false;
        bool printSeries = false;

        void pars(int argc, char **charv);
};

#endif