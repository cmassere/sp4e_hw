//
#ifndef WRITESERIES_H
#define WRITESERIES_H

#include "DumperSeries.h"
#include <fstream>
#include <string>

class WriteSeries : public DumperSeries{

public:
    WriteSeries(Series & s, unsigned int maxiter, const std::string& filename);
    void dump (std::ostream& os = std::cout) override;
    void setSeparator (const std::string& sep);
    void setPrecision(unsigned int precision) override;

private:
    unsigned int maxIterations;
    std::string outputFilename;
    std::string separator = " "; // Default is space
    std::string getFileExtension() const; // Helper function to determine file extension
};

#endif //WRITESERIES_H
