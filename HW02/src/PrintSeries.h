#ifndef PRINT_SERIES_H
#define PRINT_SERIES_H

#include "DumperSeries.h"
#include <iostream>

class PrintSeries : public DumperSeries {
public:
    PrintSeries(Series& s, unsigned int frequency, unsigned int maxiter)
            : DumperSeries(s), freq(frequency), maxIterations(maxiter) {}
    void dump(std::ostream & os = std::cout) override;
    void setPrecision (unsigned int precision) override;

private:
    unsigned int freq;
    unsigned int maxIterations;
    unsigned int m_precision;
};

#endif // PRINT_SERIES_H
