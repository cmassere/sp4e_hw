#include "Series.h"

Series::Series()
{
    this->restart();
}

SeriesReal Series::computeNextNSteps(SeriesInt n)
{
    for(SeriesInt n_idx = 0;n_idx<n;n_idx++)
    {
        this->computeNextStep();
    }

    return this->getActualValue();
}

SeriesReal Series::compute(SeriesInt n)
{
    this->restart();
    this->computeNextNSteps(n);

    return this->getActualValue();
}

void Series::restart(void)
{
    this->current_index = 0;
    this->current_value = 0;
}