import matplotlib.pyplot as plt
import pandas as pd
import sys
import os
import numpy as np

# Show files in current directory
print(os.listdir('.'))
# Show files in parent directory
print(os.listdir('..'))


csv_file_path = 'cmake-build-debug/src/result.csv'


# Check if file exists
if not os.path.isfile(csv_file_path):
    print("File path {} does not exist. Exiting...".format(csv_file_path))
    sys.exit(1)


# Initialize an empty DataFrame
df = pd.DataFrame(columns=['Step', 'Value'])

# Variable to hold the Analytic Prediction
analytic_prediction = None

# Read the file line by line
with open(csv_file_path, 'r') as file:
    lines = file.readlines()

    # Process each line
    for line in lines:
        if 'Analytic Prediction' in line:
            analytic_prediction = float(line.split()[-1])
        else:
            step, value = line.split()[1:3]
            new_row = pd.DataFrame({'Step': [int(step)], 'Value': [float(value)]})
            df = pd.concat([df, new_row], ignore_index=True)


# Now df has all the numerical step results and we have the analytic prediction separately
# Plotting
plt.plot(df['Step'], df['Value'], label='Numerical Results')

# If there's an analytic prediction, plot it
if analytic_prediction is not None:
    plt.axhline(y=analytic_prediction, color='r', linestyle='--', label=f'Analytic Prediction ({analytic_prediction})')

plt.xlabel('Step')
plt.ylabel('Value')
plt.title('Convergence of Series to π')
plt.legend()
plt.show()