#include "ComputePi.h"
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265
#endif



SeriesReal ComputePi::computeNextStep(void)
{
    this->current_index += 1;
    this->current_value += 1./SeriesReal((this->current_index)*(this->current_index));

    return this->getActualValue();
}

SeriesReal ComputePi::getActualValue(void)
{
    return sqrt(6*this->current_value);
}

ComputePi::ComputePi():Series()
{}

SeriesReal ComputePi::getAnalyticPrediction() const {
    return M_PI;  //
}
