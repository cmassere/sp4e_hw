#include <cstdlib>
#include <iostream>
#include <sstream>
#include <filesystem>
#include <memory>

#include "ComputeArithmetic.h"
#include "ComputePi.h"
#include "RiemannIntegral.h"
#include "PrintSeries.h"
#include "WriteSeries.h"

#include <math.h>
#include <string>
#include <vector>
#include "ArgPars.h"

/* -------------------------------------------------------------------------- */

// definition of 3 dg curve for Riemann integral
SeriesReal fct_3dg(SeriesReal x)
{
    return x*x*x;
}

int main(int argc, char **charv)
{
    ArgPars* myParser = new ArgPars();
    myParser->pars(argc,charv);

    Series* selectedSeries;
    switch (myParser->seSel)
    {
        case _se_arithmetic:
            selectedSeries = new ComputeArithmetic();
            break;
        case _se_pi:
            selectedSeries = new ComputePi();
            break;
        case _se_riemann:
            switch (myParser->fctSel)
            {
                case _fct_3dg:
                    selectedSeries = new RiemannIntegral(&fct_3dg,myParser->a,myParser->b);
                    break;
                case _fct_sin:
                    selectedSeries = new RiemannIntegral(&sin,myParser->a,myParser->b);
                    break;
                case _fct_cos:
                    selectedSeries = new RiemannIntegral(&cos,myParser->a,myParser->b);
                    break;
            }
            break;
    }

    if(myParser->printSeries)
    {
        PrintSeries* ps = new PrintSeries(*selectedSeries,myParser->freq,myParser->n);
        std::cout << *ps;
    }  
    if(myParser->writeSeries)
    {
        WriteSeries* ws = new WriteSeries(*selectedSeries,myParser->n, "ver01");
        ws->setSeparator(myParser->dumpSeparator);
        std::cout << *ws;
    }
    return EXIT_SUCCESS;
}
