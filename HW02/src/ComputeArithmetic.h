#include "Series.h"

#ifndef COMPUTEARITHMETIC_H
#define COMPUTEARITHMETIC_H

class ComputeArithmetic:public Series
{
    public:
        ComputeArithmetic();
        SeriesReal computeNextStep(void) override;
        SeriesReal getActualValue(void) override;
};

#endif