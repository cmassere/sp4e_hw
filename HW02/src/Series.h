#ifndef SERIES_H
#define SERIES_H

#include <cmath> // for nan()

using SeriesInt = unsigned int;
using SeriesReal = double;

class Series
{
    protected:
        SeriesInt current_index{};
        SeriesReal current_value{};

    public:
        Series();

        void restart(void);
        

        SeriesReal computeNextNSteps(SeriesInt n);
        SeriesReal compute(SeriesInt n);

        virtual SeriesReal computeNextStep(void) = 0;
        virtual SeriesReal getActualValue(void) = 0;
        virtual SeriesReal getAnalyticPrediction() const {return  std::nan("");} // default
};

#endif