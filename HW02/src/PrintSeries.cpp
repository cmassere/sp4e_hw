#include "PrintSeries.h"

void PrintSeries::setPrecision(unsigned int precision) {
    m_precision = precision;
}

void PrintSeries::dump(std::ostream & os) {

    os.precision(m_precision);

    RiemannIntegral* ptr = dynamic_cast<RiemannIntegral*>(&(this->series));
    if (ptr != NULL)
    {
        ptr->setIter(this->maxIterations);
    }
    
    series.restart();
    double analyticPrediction = series.getAnalyticPrediction();
    if (!std::isnan(analyticPrediction))
        os << "Analytic Prediction: " << std::to_string(analyticPrediction) << "\n" << std::endl;

    for (unsigned int i = 0; i < maxIterations; i += freq) {
        double computedValue =series.computeNextNSteps(freq);

        os << "Step " << i << ":\t" << std::to_string(computedValue);

        // check prediction
        if (!std::isnan(analyticPrediction))
            os << "\tConvergence: " << std::to_string(std::abs(computedValue-analyticPrediction));

        os << std::endl;
    }
}