#include "ComputeArithmetic.h"

SeriesReal ComputeArithmetic::computeNextStep(void)
{
    this->current_index += 1;
    this->current_value += this->current_index;

    return this->getActualValue();
}

SeriesReal ComputeArithmetic::getActualValue(void)
{
    return this->current_value;
}

ComputeArithmetic::ComputeArithmetic():Series()
{}