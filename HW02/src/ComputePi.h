#include "Series.h"
#ifndef COMPUTEPI_H
#define COMPUTEPI_H

class ComputePi:public Series
{
    public:
        ComputePi();
        SeriesReal computeNextStep(void) override;
        SeriesReal getActualValue(void) override;
        SeriesReal getAnalyticPrediction() const override;

};

#endif