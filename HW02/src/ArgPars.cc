#include "ArgPars.h"


void ArgPars::pars(int argc, char **charv)
{
    int n_arg = 0;
    ArgParsState argSt = _APSt_none;

    for(n_arg = 1;n_arg<argc;n_arg++)
    {
        std::string actualArg(charv[n_arg]);

        if(actualArg.compare("--step")==0 || actualArg.compare("-n")==0)
        {
            argSt = _APSt_get_n;
        }
        else if (actualArg.compare("--series")==0 || actualArg.compare("-s")==0)
        {
            argSt = _APSt_series;
        }
        else if (actualArg.compare("--function")==0 || actualArg.compare("-f")==0)
        {
            argSt = _APSt_func;
        }
        else if (actualArg.compare("--write")==0 || actualArg.compare("-w")==0)
        {
            argSt = _APSt_none;
            this->writeSeries = true;
        }
        else if (actualArg.compare("--print")==0 || actualArg.compare("-p")==0)
        {
            argSt = _APSt_none;
            this->printSeries = true;
        }
        else if (actualArg.compare("--separator")==0 || actualArg.compare("-sep")==0)
        {
            argSt = _APSt_sep;
        }
        else if (actualArg.compare("--value_a")==0 || actualArg.compare("-va")==0)
        {
            argSt = _APSt_val_a;
        }
        else if (actualArg.compare("--value_b")==0 || actualArg.compare("-vb")==0)
        {
            argSt = _APSt_val_b;
        }
        else if (actualArg.compare("--frequency")==0 || actualArg.compare("-freq")==0)
        {
            argSt = _APSt_freq;
        }
        else
        {
            switch (argSt)
            {
                case _APSt_get_n:
                {
                    int tmp = int(n);
                    bool ok = true;
                    try
                    {
                        tmp = std::stoi(actualArg);
                    }
                    catch(const std::exception& e)
                    {
                        ok = false;
                    }

                    if (!ok || tmp<=0)
                    {
                        std::cout <<"ERROR '"<< actualArg << "': Incorrect number of step! Execution continue with default value." << std::endl;
                    }
                    else
                    {
                        this->n = SeriesInt(tmp);
                    }
                    break;
                }
                
                case _APSt_series:
                    if (actualArg.compare("arithmetic") == 0)
                    {
                        this->seSel = _se_arithmetic;
                    }
                    else if (actualArg.compare("pi") == 0)
                    {
                        this->seSel = _se_pi;
                    }
                    else if (actualArg.compare("riemann") == 0)
                    {
                        this->seSel = _se_riemann;
                    }
                    else
                    {
                        std::cout <<"ERROR '"<< actualArg << "': Incorrect type of series chose between 'arithmetic', 'pi', 'riemann'! Execution continue with default value." << std::endl;
                    }
                    break;

                case _APSt_func:
                    if (actualArg.compare("3dg") == 0)
                    {
                        this->fctSel = _fct_3dg;
                        if(!this->value_a_set)
                            this->a = 0;
                        if(!this->value_b_set)
                            this->b = 1;
                    }
                    else if (actualArg.compare("cos") == 0)
                    {
                        this->fctSel = _fct_cos;
                        if(!this->value_a_set)
                            this->a = 0;
                        if(!this->value_b_set)
                            this->b = M_PI;
                    }
                    else if (actualArg.compare("sin") == 0)
                    {
                        this->fctSel = _fct_sin;
                        if(!this->value_a_set)
                            this->a = 0;
                        if(!this->value_b_set)
                            this->b = M_PI/2;
                    }
                    else
                    {
                        std::cout <<"ERROR '"<< actualArg << "': Incorrect type of function chose between '3dg', 'cos', 'sin'! Execution continue with default value." << std::endl;
                    }
                    break;

                case _APSt_sep:
                {
                    std::vector<std::string> check = {","," ","\\t","\t","|"};
                    bool ok = false;
                    for(int i = 0;i<check.size();i++)
                        if (actualArg.compare(check[i])==0)
                            ok = true;

                    if (ok)
                    {
                        if (actualArg.compare("\\t")==0)
                            this->dumpSeparator = "\t";
                        else
                            this->dumpSeparator = actualArg;
                    }
                    else
                    {
                        std::cout <<"ERROR '"<< actualArg << "': Incorrect separator chose between ',', '\\t', ' ', '|'! Execution continue with default value." << std::endl;
                    }
                          
                    break;
                }
                case _APSt_val_a:
                {
                    try
                    {
                        this->a = SeriesReal(std::stod(actualArg));
                        this->value_a_set = true;
                    }
                    catch(const std::exception& e)
                    {
                        std::cout <<"ERROR '"<< actualArg << "': Incorrect a value! Execution continue with default value." << std::endl;
                    }
                    break;
                }
                case _APSt_val_b:
                    try
                    {
                        this->b = SeriesReal(std::stod(actualArg));
                        this->value_b_set = true;
                    }
                    catch(const std::exception& e)
                    {
                        std::cout <<"ERROR '"<< actualArg << "': Incorrect b value! Execution continue with default value." << std::endl;
                    }
                    break;
                case _APSt_freq:
                    try
                    {
                        this->freq = SeriesReal(std::stoi(actualArg));
                    }
                    catch(const std::exception& e)
                    {
                        std::cout <<"ERROR '"<< actualArg << "': Incorrect frequency value! Execution continue with default value." << std::endl;
                    }
                    break;

                default:
                    std::cout <<"ERROR '"<< actualArg << "': Unknown parameter. Discarded." << std::endl;

            }
            argSt = _APSt_none;
        }

    }
    if(argSt != _APSt_none)
    {
        std::cout <<"ERROR '"<< charv[argc-1] << "': unexpected ending." << std::endl;
    }
}