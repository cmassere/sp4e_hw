#ifndef DUMPER_SERIES_H
#define DUMPER_SERIES_H

#include <iostream>
#include "Series.h"
#include "RiemannIntegral.h"


class DumperSeries {

public:
    virtual void dump(std::ostream & os) = 0;
    virtual void setPrecision(unsigned int precision) = 0;
    explicit DumperSeries(Series & s);
    virtual ~DumperSeries() = default;

protected:
    Series & series;

};

// Operator overload declaration and definition
inline std::ostream & operator <<(std::ostream & os, DumperSeries & _this) {
    _this.dump(os);
    return os;
}



#endif