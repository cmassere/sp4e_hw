#include "WriteSeries.h"

WriteSeries::WriteSeries(Series& s, unsigned int maxiter, const std::string& filename)
        : DumperSeries(s), maxIterations(maxiter), outputFilename(filename) {}

void WriteSeries::setSeparator(const std::string& sep) {
    separator = sep;
    // Change the file extension based on the separator
    outputFilename = outputFilename.substr(0, outputFilename.find_last_of('.')) + getFileExtension();
}

std::string WriteSeries::getFileExtension() const {
    if (separator.compare(",")==0) return ".csv";
    if (separator.compare("|")==0) return ".psv";
    return ".txt"; // Default
}

void WriteSeries::dump(std::ostream &os) {
    std::ofstream outFile(outputFilename);

    RiemannIntegral* ptr = dynamic_cast<RiemannIntegral*>(&(this->series));
    if (ptr != NULL)
    {
        ptr->setIter(this->maxIterations);
    }

    if (!outFile) {
        throw std::runtime_error("Unable to open file: " + outputFilename);
    }

    series.restart();
    for (unsigned int i = 0; i < maxIterations; ++i) {
        double value = series.computeNextStep();
        outFile << "Step" << separator << i << separator << value << std::endl;
    }

    // Check and write the analytic prediction if it is not NaN
    double analyticPrediction = series.getAnalyticPrediction();
    if (!std::isnan(analyticPrediction)) {
        outFile << "Analytic Prediction" << separator << analyticPrediction << std::endl;
    }

    outFile.close();
}

void WriteSeries::setPrecision(unsigned int precision) {

}
