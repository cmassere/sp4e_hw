#include "RiemannIntegral.h"

RiemannIntegral::RiemannIntegral(SeriesReal(*func)(SeriesReal),SeriesReal a,SeriesReal b,SeriesInt n_iter):Series()
{
    this->func=func;
    this->a = a;
    this->b = b;
    this-> n_iter=n_iter;
    this->deltaX = (b-a)/SeriesInt(n_iter);
}

void RiemannIntegral::setFunction(SeriesReal(*func)(SeriesReal))
{
    this->func=func;
}

void RiemannIntegral::setLimit(SeriesReal a,SeriesReal b)
{
    this->a = a;
    this->b = b;
    this->deltaX = (b-a)/SeriesInt(n_iter);
}

void RiemannIntegral::setIter(SeriesInt n_iter)
{
    this-> n_iter=n_iter;
    this->deltaX = (b-a)/SeriesInt(n_iter);
}

SeriesReal RiemannIntegral::computeNextStep(void)
{
    this->current_index += 1;
    this->current_value += this->func(this->a + SeriesReal(this->current_index)*this->deltaX)*this->deltaX;
    return this->getActualValue();
}

SeriesReal RiemannIntegral::getActualValue(void)
{
    return this->current_value;
}

SeriesReal RiemannIntegral::compute(SeriesInt n)
{
    this->restart();
    this->setIter(n);
    this->computeNextNSteps(n);

    return this->getActualValue();
}